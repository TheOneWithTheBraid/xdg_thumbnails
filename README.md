# xdg_thumbnails

Access XDG thumbnails on Unix systems.

## Features

- Access thumbnails by Uri
- Get normal or large sizes of the thumbnails

## Usage

```dart
// create an [XdgThumbnail] instance
final thumbnail = XdgThumbnail(Uri.path('/my/fancy/file.extension'));

// get the normal thumbnail file
final normalThumbnail = thumbnail.normal;

if(normalThumbnail == null) {
  print('There\'s no thumbnail for the fancy file.');
}

print(normalThumbnail.normal?.readAsBytesSync());

// or you might fetch the large thumbnail
final largeThumbnail = thumbnail.large;
```

## Additional information

Theoretically, the [XDG Thumbnail Managing Standard](https://specifications.freedesktop.org/thumbnail-spec/thumbnail-spec-latest.html)
allows "Extra", as well as "Extra extra" large thumbnails too, but in real life, it does not seem as if they are used.
As a result, this package does not support them to avoid confusion.

This package is [EUPL](LICENSE) licensed.