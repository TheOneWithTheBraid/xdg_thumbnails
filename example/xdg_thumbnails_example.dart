import 'package:xdg_thumbnails/xdg_thumbnails.dart';

void main(List<String> args) {
  assert(args.length == 1,
      'Please provide one argument representing an absolute file path.');

  // creating the [XdgThumbnail] for the given file
  var thumbnail = XdgThumbnail(Uri.file(args[0]));

  print('Normal thumbnail path: ${thumbnail.normal?.path}');
  print('Large thumbnail path: ${thumbnail.normal?.path}');

  print(
      'Normal thumbnail Uint8List (beginning): ${thumbnail.normal?.readAsBytesSync().getRange(0, 50).toString()}');
}
