import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:xdg_directories/xdg_directories.dart';

/// Creates an identifier for an XDG thumbnail from a provided file [Uri]
class XdgThumbnail {
  late final Uri _source;

  /// the computed thumbnail file in XDG's cache
  late final String _thumbnailHash;

  XdgThumbnail(
    /// the [Uri] of the file you want to get a thumbnail from
    Uri file,
  ) {
    _source = file;
    final codec = Utf8Codec();
    _thumbnailHash =
        md5.convert(codec.encode(_source.toString())).toString().toLowerCase();
  }

  /// fetches the XDG thumbnail for the current file by a given size type
  /// returns [null] in case there is no thumbnail available
  File? _thumbnailPathByType(String type) {
    final file = File(
        cacheHome.path + '/' + 'thumbnails/$type/' + _thumbnailHash + '.png');

    // returning the file if it exists
    if (file.existsSync()) {
      return file;
    }

    // checking whether there are relative thumbnails available
    // (e.g. on removable media)
    final segments =
        _source.pathSegments.getRange(0, _source.pathSegments.length);
    final relativeThumbnailFile = File('/' +
        segments.join('/') +
        '/.sh_thumbnails/$type/' +
        _thumbnailHash +
        '.png');

    // returning the relative file if it exists
    if (relativeThumbnailFile.existsSync()) {
      return relativeThumbnailFile;
    }
  }

  /// returns the normally sized thumbnail [File]
  /// returns [null] in case there is no thumbnail available
  File? get normal => _thumbnailPathByType('normal');

  /// returns the large thumbnail [File]
  /// returns [null] in case there is no thumbnail available
  File? get large => _thumbnailPathByType('large');
}
