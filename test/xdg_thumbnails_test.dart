import 'package:test/test.dart';
import 'package:xdg_thumbnails/xdg_thumbnails.dart';

void main() {
  group('Unavailable:', () {
    test('Checking for a random file', () {
      final thumbnail =
          XdgThumbnail(Uri.file('/tmp/xdg_thumbnails/does_not_exist'));
      expect(thumbnail.normal, null);
    });
  });
}
